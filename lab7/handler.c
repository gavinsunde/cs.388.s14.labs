/* handler.c: HTTP Request Handlers */

#include "chippewa.h"

#include <errno.h>
#include <limits.h>
#include <string.h>

#include <dirent.h>
#include <unistd.h>

/* Internal Declarations */
http_status_t handle_browse_request(struct request_t *request);
http_status_t handle_file_request(struct request_t *request);
http_status_t handle_cgi_request(struct request_t *request);
http_status_t handle_error(struct request_t *request, http_status_t status);

/** 
 * Handle HTTP Request
 *
 * This parses a request, determines the request path, determines the request
 * type, and then dispatches to the appropriate handler type.
 *
 * On error, handle_error should be used with an appropriate HTTP status code.
 **/
http_status_t
handle_request(struct request_t *r)
{
    http_status_t result;

    /* Parse request */
    /* TODO */

    /* Determine request path */
    /* TODO */

    /* Dispatch to appropriate request handler type */
    /* TODO */

    log("HTTP REQUEST STATUS: %s", http_status_string(result));
    return (result);
}

/**
 * Handle browse request
 *
 * This lists the contents of a directory in HTML.
 *
 * If the path cannot be opened or scanned as a directory, then handle error 
 * with HTTP_STATUS_NOT_FOUND.
 **/
http_status_t
handle_browse_request(struct request_t *r)
{
    struct dirent **entries;
    int n;

    /* Open a directory for reading or scanning */
    /* ODO */
    entries = opendir(r->path);
    /* Write HTTP Header with OK Status and text/html Content-Type */
    /* ODO */
	char *header = "HTTP/1.0 200 OK\n content-Type: text/html";
	send(r->fd, header, strlen(header), 0);
    /* For each entry in directory, emit HTML list item */
    /* ODO */
	char *htmlList;
        send(r->fd, "<html>", 7, 0);
	while ((entries = readdir(entries)) != NULL) {
		htmlList = entries->d_name;
		send(r->fd, htmlList, strlen(htmlList), 0);
	}
        send(r->fd, "</html>", 8, 0);
    /* FLUSH */
    /* ODO */
	fflush();	
    return (HTTP_STATUS_OK);
}

/**
 * Handle file request
 *
 * This opens and streams the contents of the specified file to the socket.
 *
 * If the path cannot be opened for reading, then handle error with
 * HTTP_STATUS_NOT_FOUND.
 **/
http_status_t
handle_file_request(struct request_t *r)
{
    FILE *fs;
    char buffer[BUFSIZ];
    char *mimetype = NULL;
    size_t nread;

    /* Open file for reading */
    /* ODO */
    fs = fopen(r->path, "r");
    /* Determine mimetype */
    /* ODO */
	mimetype = determine_mimetype(r->path);
    /* Write HTTP Headers with OK status and determined Content-Type */
    /* ODO */
	string *header = "HTTP/1.0 200 OK\n content-Type: ";
	strcat(header, mimetype);
	send(r->fd, header, strlen(header), 0);
    /* Read from file and write to socket in chunks */
    /* ODO */
    fgets(buffer, BUFSIZ, fs);
	while (!feof(fs)) {
		send(r->fd, buffer, strlen(buffer), 0);
		fgets(buffer, BUFSIZ, fs);
	}
    /* Close file, flush socket, deallocate mimetype, return OK */
    /* ODO */
	fclose(fs);
	free(mimetype);
	fflush();
    return (HTTP_STATUS_OK);

fail:
    /* Close file, free mimetype, return INTERNAL_SERVER_ERROR */
    /* ODO */
	fclose(fs);
	free(mimetype);
    return (HTTP_STATUS_INTERNAL_SERVER_ERROR);
}

/**
 * Handle file request
 *
 * This popens and streams the results of the specified executables to the
 * socket.
 *
 * If the path cannot be popened, then handle error with
 * HTTP_STATUS_INTERNAL_SERVER_ERROR.
 **/
http_status_t
handle_cgi_request(struct request_t *r)
{
    FILE *pfs;
    char buffer[BUFSIZ];
    struct header_t *header;

    /* Export CGI environment variables from request:
     * http://en.wikipedia.org/wiki/Common_Gateway_Interface */
    /* TODO */
    
    /* Export CGI environment variables from request headers */
    /* TODO */

    /* POpen CGI Script */
    /* ODO */
    pfs = popen(r->path);
    /* Copy data from popen to socket */
    /* ODO */
        fgets(buffer, BUFSIZ, fs);
        while (!feof(pfs)) {
                send(r->fd, buffer, strlen(buffer), 0);
                fgets(buffer, BUFSIZ, pfs);
        }
    /* Close popen, flush socket, return OK */
    /* ODO */
	pclose(pfs);
	fflush();
    return (HTTP_STATUS_OK);
}

/**
 * Handle displaying error page
 *
 * This writes an HTTP status error code and then generates an HTML message to
 * notify the user of the error.
 **/
http_status_t
handle_error(struct request_t *r, http_status_t status)
{
    const char *status_string = http_status_string(status);

    /* Write HTTP Header */
    /* ODO */
    char *header = "HTTP/1.0 ";
    strcat(header, status_string);
    strcat(header, "\nContent-Type: text/html\n");
	send(r->fd, header, strlen(header), 0);
    /* Write HTML Description of Error*/
    /* ODO */
    char *errstr = "<html> There was an error:\n";
	send(r->fd, errstr, strlen(errstr), 0);
	send(r->fd, status_string, strlen(status_string), 0);
    strcat(errstr, "\nSo no fun times?? \n<\\html>");
        send(r->fd, errstr, strlen(errstr), 0);
    /* Return specified status */
    return (status);
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
