/* request.c: HTTP Request Functions */

#include "chippewa.h"

#include <errno.h>
#include <string.h>

#include <unistd.h>

int parse_request_method(struct request_t *r);
int parse_request_headers(struct request_t *r);

/**
 * Accept request from server socket.
 *
 * This function does the following:
 *
 *  1. Allocates a request struct initialized to 0.
 *  2. Initializes the headers list in the request struct.
 *  3. Accepts a client connection from the server socket.
 *  4. Looks up the client information and stores it in the request struct.
 *  5. Opens the client socket stream for the request struct.
 *  6. Returns the request struct.
 *
 * The returned request struct must be deallocated using free_request.
 **/
struct request_t *
accept_request(int sfd)
{
    struct request_t *r;
    struct sockaddr raddr;
    socklen_t rlen;

    /* Allocate request struct (zeroed) */
    /* ODO */
	r = malloc(sizeof(struct request_t));
    /* Initialize headers */
    /* TODO */
	r->headers = malloc(sizeof(struct headers_t));
    /* Accept a client */
    /* TODO */
	accept(sfd, raddr, rlen)
    /* Lookup client information */
    /* TODO */
	getaddrinfo(NULL, sdf, raddr, raddr);
    /* Open socket stream */
    /* TODO */
	

    log("Accepted request from %s:%s", r->host, r->port);
    return (r);

fail:
    free_request(r);
    return (NULL);
}

/**
 * Deallocate request struct.
 *
 * This function does the following:
 *
 *  1. Closes the request socket stream or file descriptor.
 *  2. Frees all allocated strings in request struct.
 *  3. Frees all of the headers (including any allocated fields).
 *  4. Frees request struct.
 **/
void
free_request(struct request_t *r)
{
    struct header_t *header;

    if (r == NULL) {
    	return;
    }

    /* Close socket or fd */
    /* ODO */
	close(r->fd);
    /* Free allocated strings */
    /* ODO */
	free(r->method);
	free(r->uri);
	free(r->path);
	free(r->query);
	free(r->host);
	free(r->port);
    /* Free headers */
    /* ODO */
	TAILQ_EMPTY(r->headers);
    /* Free request */
    /* ODO */
	free(r);
}

/**
 * Parse HTTP Request.
 *
 * This function first parses the request method, any query, and then the
 * headers, returning 0 on success, and -1 on error.
 **/
int
parse_request(struct request_t *r)
{
    /* Parse HTTP Request Method */
    /* TODO */

    /* Parse HTTP Requet Headers */
    /* TODO */
}

/**
 * Parse HTTP Request Method and URI
 *
 * HTTP Requests come in the form
 *
 *  <METHOD> <URI>[QUERY] HTTP/<VERSION>
 *
 * Examples:
 *
 *  GET / HTTP/1.1
 *  GET /cgi.script?q=foo HTTP/1.0
 *
 * This function extracts the method, uri, and query (if it exists).
 **/
int
parse_request_method(struct request_t *r)
{
    char buffer[BUFSIZ];
    char *method;
    char *uri;
    char *query;

    /* Read line from socket */
    /* ODO */
	recv(r->fd, buffer, BUFSIZ, 0);
    /* Parse method and uri */
    /* ODO */
	strcpy(method, buffer);
	method = strtok(method, " ");
    /* Parse query from uri */
    /* ODO */
	query = strtok(query, " ");
	uri = strtok(uri, "\r\n");
    /* Record method, uri, and query in request struct */
    /* ODO */
	r->method = method;
	r->uri = uri;
	r->query = query;
    debug("HTTP METHOD: %s", r->method);
    debug("HTTP URI:    %s", r->uri);
    debug("HTTP QUERY:  %s", r->query);

    return (0);

fail:
    return (-1);
}

/**
 * Parse HTTP Request Headers
 *
 * HTTP Headers come in the form:
 *
 *  <NAME>: <VALUE>
 *
 * Example:
 *
 *  Host: localhost:8888
 *  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:29.0) Gecko/20100101 Firefox/29.0
 *  Accept: text/html,application/xhtml+xml
 *  Accept-Language: en-US,en;q=0.5
 *  Accept-Encoding: gzip, deflate
 *  Connection: keep-alive
 *
 * This function parses the stream from the request socket using the following
 * pseudo-code:
 *
 *  while (buffer = read_from_socket() and buffer is not empty):
 *      name, value = buffer.split(':')
 *      header      = new Header(name, value)
 *      headers.append(header)
 **/
int
parse_request_headers(struct request_t *r)
{
    struct header_t *header;
    char buffer[BUFSIZ];
    char *name;
    char *value;
    
    /* Parse headers from socket */
    /* ODO */
	while (recv(r->fd, buffer, BUFSIZ, 0) && buffer != NULL) {
		strcpy(name, buffer);
		strtok(name, ":");
		strtok(value, "\0");
		header = new Header(name, value);
		TAILQ_INSERT_TAIL(%r->headers, header, headers):
	}
#ifndef NDEBUG
    TAILQ_FOREACH(header, &r->headers, headers) {
    	debug("HTTP HEADER %s = %s", header->name, header->value);
    }
#endif
    return (0);

fail:
    return (-1);
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
