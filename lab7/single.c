/* single.c: Single User HTTP Server */

#include "chippewa.h"

#include <errno.h>
#include <string.h>

#include <unistd.h>

/**
 * Handle one HTTP request at a time
 **/
void
single_server(int sfd)
{
    struct request_t *request;

    /* Accept and handle HTTP request */
    while (1) {
    	/* Accept request */
    	/* ODO */
	request = accept_request(sfd);
	/* Handle request */
    	/* ODO */
	printf("%s\n", http_status_string(handle_request(request)));
	/* Free request */
    	/* ODO */
	free_request(request);
    }

    /* Close server socket */
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
