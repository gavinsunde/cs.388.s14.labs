#!/bin/bash

usage() { echo "Usage: $0 [-f <options>] locations" 1>&2; exit 1; }

while getopts ":f:" o; do
    case "${o}" in
	f)
	    f=${OPTARG}
	    ;;
	*)
	    usage
	    ;;
    esac
done
shift $((OPTIND-1))


locv=$@
for word in $locv
do
    echo $word
    curl -s http://w1.weather.gov/xml/current_obs/$word.xml > temp.xml
    for field in $(echo $f | tr "," " "); do
	#echo $field
	grep \<$field temp.xml |sed s/\<$field\>/""/ |sed s/..$field\>/""/
    done
    rm temp.xml
    

done
