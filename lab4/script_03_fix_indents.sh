#!/bin/bash


while [ "$1" != "" ]; do
	case $1 in
		-i )		shift
				i=$1
				;;
		-w )		shift
				num=$1
				;;
		* )		echo "ERROR $1 not valid argument"
				exit
				;;
	esac
	shift
done

if [ "$i" = "spaces" ]; then
	sed 's/^	/ /'
elif [ "$i" = "tabs" ]; then
	sed 's/^ /	/'
else
	echo "ERROR USAGE -i [spaces|tabs]"
fi
