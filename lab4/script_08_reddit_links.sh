#!/bin/bash
echo
echo
echo "Submissions from $1:"
echo
echo
curl -s $1/.json | python -m json.tool |
awk -F"\"" 'BEGIN { count=1;}
/\"title\":/ { print count ". " $4; count++; }
/\"url\":/ { print "    "$4"\n"; }'
#END


