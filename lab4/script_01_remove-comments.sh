#!/bin/bash

delim=#
Wspace=false

while [ "$1" != "" ]; do
	case $1 in
		-c )		shift
				delim=$1
				;;
		-W )		Wspace=true
				;;
		* )		echo "ERROR $1 not valid argument"
				exit
				;;
	esac
	shift
done

echo "$delim"
echo $Wspace

if [ "$Wspace" = "true" ]; then
		sed "s/$delim .*//" |
		sed '/^$/d'
	else
		sed "s/$delim .*//"
	fi



