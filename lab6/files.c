/* files.c: rorschach files */

#include "rorschach.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * Add file to files list with specified path, mtime, and timestamp.
 */
void
add_file(struct files_t *files, const char *path, const time_t mtime, const time_t timestamp)
{
    struct file_t *file = malloc(sizeof(struct file_t));

    /* ODO: Allocate file and set fields */
    file->path = strdup(path);
    file->mtime = mtime;
    file->timestamp = timestamp;

    /* ODO: Insert file into files list */
    TAILQ_INSERT_TAIL(files, file, filesE);

    debug("ADD FILE: path=%s, mtime=%lu, timestamp=%lu", path, mtime, timestamp);
}

/**
 * Search files list for specified path.
 *
 * Return file if found, otherwise NULL.
 */
struct file_t *
search_files(struct files_t *files, const char *path)
{
    /* ODO */
    struct file_t *file = malloc(sizeof(struct file_t));
    TAILQ_FOREACH(file, files, filesE);
        if (true) {
		return (file);
        }
    return (NULL);
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
