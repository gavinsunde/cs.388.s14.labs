/* utils.c: rorschach utilities */

#include "rorschach.h"

#include <ctype.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>

#include <sys/stat.h>
#include <sys/time.h>

/* Utilities */

/**
 * Return new string containing current timestamp from gettimeofday
 */
char *
timestamp_string()
{
    char timestamp[TIMESTAMP_MAX];
    struct timeval tv;

    /* ODO */
	gettimeofday(&tv, NULL);
	time_t curtime = tv.tv_sec;
	strftime(timestamp, 20, "%Y-%m-%d %H:%M:%S", localtime(&curtime));
    return (strdup(timestamp));
}

/**
 * Return new string containing base path of path
 */
char *
basepath_string(const char *path)
{
    char buffer[BUFSIZ];

    /* ODO */
	strcpy(buffer, path);
	strtok(buffer, ".");
	
    return (strdup(buffer));
}

/**
 * Advance string pointer pass all nonwhitespace characters
 */
char *
skip_nonwhitespace(char *s)
{
    /* ODO */
    while (*s && !isspace(*s)) {
        s++;
    }
    return (s);
}

/**
 * Advance string pointer pass all whitespace characters
 */
char *
skip_whitespace(char *s)
{
    /* ODO */
    while (*s && isspace(*s)) {
        s++;
    }	
    return (s);
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
