/* middleman - reads from named pipe and wirtes to a process */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>


FILE *
safe_popen(const char *command, const char *type) 
{
	FILE *sfp;

	sfp = popen(command, type);
	if (sfp == NULL) {
		fprintf(stderr, "unable to popen %s: %s\n", command, strerror(errno));
		exit(EXIT_FAILURE);
	}
	return sfp;
}

void
sendCommand(char *command, char *fifoPath, bool toStdout)
{
	FILE *sfp;
	sfp = safe_popen(command, "w");
	FILE *fifofp;
	fifofp = fopen(fifoPath, "r");

	char fifoContent[BUFSIZ];
	fgets(fifoContent, BUFSIZ, fifofp);
	
	while (!feof(fifofp)) {
	fprintf(sfp, fifoContent);
	if (toStdout) {
		printf(fifoContent);
	}
	fgets(fifoContent, BUFSIZ, fifofp);
}

	pclose(sfp);
}

int
main(int argc, char *argv[])
{
	int c;
	char *fifoPath;
	char *command;
	bool toStdout = false;
	
	while ((c = getopt(argc, argv, "h,E,c:f:")) != -1) {
		switch (c) {
			case 'h':
				printf("usage: middleman [-E] -c command -f fifo_path\n");
				return (EXIT_SUCCESS);
			case 'E':
				toStdout = true;
				break;
			case 'c':
				command = optarg;
				break;
			case 'f':
				fifoPath = optarg;
				break;
			default:
				fprintf(stderr, "%s: unknown arg %c\n", argv[0], c);
				return (EXIT_FAILURE);
		}
	}
	sendCommand(command, fifoPath, toStdout);
	return (EXIT_SUCCESS);

}
