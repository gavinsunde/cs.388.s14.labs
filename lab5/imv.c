/****

     imv.c by Andrew Rens
    
This program renames files passsed to it via command line arguments.
Due to being extremely sick and unable to code for much of the last 2 weeks, 
some advanced features are missing, such as wildcard support, and handling 
invalid filenames. Therefore, it is ***important*** to make sure that the file
names entered match up with actually files on the system. If the file names are
entered correctly, the program will successfully copy them and remove the old 
version.

 ***/


#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>


int copy_file(const char *src, const char *dst)
{
  //Check if the filenames are different before copying. No need to do extra work..
  if(strcmp(src, dst) != 0){
    FILE * rfd;
    FILE * wfd;
    
    rfd = fopen(src, "r");
    wfd = fopen(dst, "w+");
   
    //Copy the files
    int c;
    while((c = fgetc(rfd)) != EOF){
      fputc(c, wfd);
    }
    
    //Close both files.
    fclose(rfd);
    fclose(wfd);
    
    //Remove the old file
    char *rmfull = (char*)(malloc(strlen(src) + 4));
    strcpy(rmfull, "rm ");
    strcat(rmfull, src);
    //printf("rm command: %s\n", rmfull);
    system(rmfull);
  }
}


int main(int argc, char *argv[]) {
  
  //Get the system editor
  char *editor = getenv("EDITOR");
  printf("%s is the system editor.\n", editor);

  //printf("%d arguments available.\n", argc); 
  
  //Open the tmp file
  FILE *tmp;
  tmp = fopen("imv.tmp", "w+");
  
  //Write the temp file
  for(int i=1; i<argc; i++){
    //intf("File: %s\n", argv[i]);
    fprintf(tmp, argv[i]);
    fprintf(tmp, "\n");
  }
  fclose(tmp);


  //Edit the temp file
  char *sys_arg;
  sys_arg = (char*)malloc(strlen(editor)+9);
  strcpy(sys_arg, editor);
  strcat(sys_arg, " imv.tmp");
  printf("Sys_arg: %s\n", sys_arg);
  system(sys_arg);
  //printf("Finished editing.\n");

  //Reopen the temp file
  tmp = fopen("imv.tmp", "r");
  int i=1; 

  //Move the files
  while(i < (argc)){
    char tmpname[64];
    fscanf(tmp, "%s", tmpname);
    //printf("Old Name: %s, New Name: %s\n", argv[i], tmpname);
    int retval = copy_file(argv[i], tmpname);
    i++;
  }

  //Remove the temp file
  system("rm imv.tmp");
  return 0;
}
