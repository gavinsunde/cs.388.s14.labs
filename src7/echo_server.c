/* echo_server: Simple Echo Server */

#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#ifdef NDEBUG
#define debug(M, ...)
#else
#define debug(M, ...)	fprintf(stderr, "DEBUG %s:%d: " M "\n", __FILE__, __LINE__, ##__VA_ARGS__)
#endif

#define ECHO_PORT   "9911"

int socket_listen(const char *port);

int
main(int argc, char *argv[])
{
    char *port;
    int  sfd;

    /* Parse command line options */
    if (argc != 2) {
    	fprintf(stderr, "usage: echo_server port\n");
    	exit(EXIT_FAILURE);
    }

    port = argv[1];
    sfd  = socket_listen(port);
    if (sfd < 0) {
    	fprintf(stderr, "Unable to listen on %s: %s\n", port, strerror(errno));
    	exit(EXIT_FAILURE);
    }

    /* Accept and handle incoming connections */
    while (true) {
    	struct sockaddr caddr;
    	socklen_t clen;
    	int cfd;
    	char chost[NI_MAXHOST], cport[NI_MAXSERV];
    	FILE *csock;
    	char buffer[BUFSIZ];

    	/* Accept a client */
    	clen = sizeof(caddr);
    	cfd  = accept(sfd, &caddr, &clen);
    	if (cfd < 0) {
    	    fprintf(stderr, "Unable to accept client: %s\n", strerror(errno));
    	    continue;
	}

	/* Lookup client information */
	if (getnameinfo(&caddr, clen, chost, sizeof(chost), cport, sizeof(cport), NI_NUMERICHOST | NI_NUMERICSERV) != 0) {
    	    fprintf(stderr, "Unable to lookup client: %s\n", strerror(errno));
    	    goto fail;
	}

	printf("[%s] connected on %s\n", chost, cport);

	/* Open socket stream */
	csock = fdopen(cfd, "r+");
	if (csock < 0) {
    	    fprintf(stderr, "Unable to open file descriptor: %s\n", strerror(errno));
    	    goto fail;
	}

	/* Read from client, log to stdout, write to client */
	while (true) {
	    /* Read from client */
	    if (fgets(buffer, BUFSIZ, csock) == NULL) {
	    	break;
	    }

	    /* Log to stdout */
	    printf("[%s] %s", chost, buffer);

	    /* Echo to client */
	    if (fputs(buffer, csock) == EOF) {
	    	break;
	    }
	}

fail:
	printf("[%s] disconnected\n", chost);
	fclose(csock);
    }

    close(sfd);
    return (EXIT_SUCCESS);
}

int
socket_listen(const char *port)
{
    struct addrinfo  hints;
    struct addrinfo *results;
    int    socket_fd = -1;

    /* Lookup server address information */
    memset(&hints, 0, sizeof(hints));
    hints.ai_family   = AF_UNSPEC;   /* Return IPv4 and IPv6 choices */
    hints.ai_socktype = SOCK_STREAM; /* Use TCP */
    hints.ai_flags    = AI_PASSIVE;  /* Use all interfaces */

    if (getaddrinfo(NULL, port, &hints, &results) < 0) {
	return (-1);
    }

    /* For each server entry, allocate socket and try to connect */
    for (struct addrinfo *p = results; p != NULL; p = p->ai_next) {
	/* Allocate socket */
	if ((socket_fd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) < 0) {
	    debug("Unable to make socket: %s", strerror(errno));
	    continue;
	}

	/* Bind socket */
	if (bind(socket_fd, p->ai_addr, p->ai_addrlen) < 0) {
	    debug("Unable to bind: %s", strerror(errno));
	    close(socket_fd);
	    continue;
	}

    	/* Listen to socket */
	if (listen(socket_fd, SOMAXCONN) < 0) {
	    debug("Unable to listen: %s", strerror(errno));
	    close(socket_fd);
	    continue;
	}

	goto success;
    }

    socket_fd = -1;

success:
    freeaddrinfo(results);
    return (socket_fd);
}
